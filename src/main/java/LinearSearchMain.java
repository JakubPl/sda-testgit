public class LinearSearchMain {
    public static void main(String[] args) {
        int[] arrayToSearchIn = {77, 88, 21, 5, 6, 9};
        int searchingFor = 9;
        for(int i = 0; i < arrayToSearchIn.length; i ++) {
            if(arrayToSearchIn[i] == searchingFor) {
                System.out.printf("Znalazlem! Element jest na miejscu %d%n", i);
                break;
            }
        }
    }
}
